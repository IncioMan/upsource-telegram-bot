# UpSource Telegram Bot

##### This is a server side UpSource Telegram Bot, ready to be executed locally or wherever you want. Just follow the instructions and link your UpSource installation to it.

---

### To run the app with Docker:

* Go to the root directory of the project and execute a Maven package:

    `mvn package`

---

* Go to into the properties folder and edit the <b>user.properties</b> file with key=value.
   

    The key must be the username used in UpSource and the value his chatid of telegram.
    
    For example:
    
        alex=123456789

    Not sure how to get the chat-id? Write to <a href=https://t.me/ChatIDBot>this bot</a>
    
---
    
* Bots cannot start a chat with users, so you need to get your users to start a chat with the bot in order to make this work.
    
   

    Just write something to <a href=https://t.me/upsource_bot>this bot</a>
    
---

* Build the container: 

    `docker build -t upsource-bot <path to the Docker file>`
    
---
   
   
* Run the container:

    `docker run -d --rm --name ub -p 8082:8080 upsource-bot`
    
---

* Add a webhook to the UpSource installation pointing to:

    `<docker-machine ip>:8082`
