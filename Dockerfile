FROM openjdk:8
MAINTAINER incio

COPY ./target/upsource-telegram-bot-0.0.1-SNAPSHOT.jar /
COPY ./properties /properties

ENV UPSOURCE.INSTALLATION-URL="http://pctiziano.adcgroup.domain:8080" \
    BOT.TOKENID="302172773:AAEbgYxVweVMOUqM1zY-CCo880jk9zDDXWA" \
	CONFIGURATION_FILE_PATH="/properties"

EXPOSE 8080

CMD ["java", "-jar", "upsource-telegram-bot-0.0.1-SNAPSHOT.jar"]
