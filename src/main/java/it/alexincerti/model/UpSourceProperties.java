package it.alexincerti.model;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "upsource")
public class UpSourceProperties {
	/**
	 * The url of the upsource installation
	 */
	private String installationUrl;

	@PostConstruct
	public void initialize() {
		System.out.println("Installation URL: " + installationUrl);
	}

	public String getInstallationUrl() {
		return installationUrl;
	}

	public void setInstallationUrl(String installationUrl) {
		this.installationUrl = installationUrl;
	}
}
