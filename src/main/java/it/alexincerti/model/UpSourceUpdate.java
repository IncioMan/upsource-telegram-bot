package it.alexincerti.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.alexincerti.model.UpSourceUpdate.Data.Base.Participant;

public final class UpSourceUpdate {
	public final long majorVersion;
	public final long minorVersion;
	public final String projectId;
	public final String dataType;
	public final Data data;

	@JsonCreator
	public UpSourceUpdate(@JsonProperty("majorVersion") long majorVersion,
			@JsonProperty("minorVersion") long minorVersion, @JsonProperty("projectId") String projectId,
			@JsonProperty("dataType") String dataType, @JsonProperty("data") Data data) {
		this.majorVersion = majorVersion;
		this.minorVersion = minorVersion;
		this.projectId = projectId;
		this.dataType = dataType;
		this.data = data;
	}

	public static final class Data {
		public final Base base;
		public final long notificationReason;
		public final String discussionId;
		public final String commentId;
		public final String commentText;
		public final Participant participant;
		public final long role;
		public final long oldState;
		public final long newState;

		@JsonCreator
		public Data(@JsonProperty("base") Base base, @JsonProperty("notificationReason") long notificationReason,
				@JsonProperty("discussionId") String discussionId, @JsonProperty("commentId") String commentId,
				@JsonProperty("commentText") String commentText, @JsonProperty("participant") Participant participant,
				@JsonProperty("role") long role, @JsonProperty("oldState") long oldState,
				@JsonProperty("newState") long newState) {
			this.base = base;
			this.notificationReason = notificationReason;
			this.discussionId = discussionId;
			this.commentId = commentId;
			this.commentText = commentText;
			this.participant = participant;
			this.role = role;
			this.oldState = oldState;
			this.newState = newState;
		}

		public static final class Base {
			public final UserId userIds[];
			public final long reviewNumber;
			public final String reviewId;
			public final long date;
			public final Actor actor;
			public final String feedEventId;

			@JsonCreator
			public Base(@JsonProperty("userIds") UserId[] userIds, @JsonProperty("reviewNumber") long reviewNumber,
					@JsonProperty("reviewId") String reviewId, @JsonProperty("date") long date,
					@JsonProperty("actor") Actor actor, @JsonProperty("feedEventId") String feedEventId) {
				this.userIds = userIds;
				this.reviewNumber = reviewNumber;
				this.reviewId = reviewId;
				this.date = date;
				this.actor = actor;
				this.feedEventId = feedEventId;
			}

			public static final class UserId {
				public final String userId;
				public final String userName;
				public final String userEmail;

				@JsonCreator
				public UserId(@JsonProperty("userId") String userId,
						@JsonProperty(value = "userName", required = false) String userName,
						@JsonProperty(value = "userEmail", required = false) String userEmail) {
					this.userId = userId;
					this.userName = userName;
					this.userEmail = userEmail;
				}
			}

			public static final class Actor {
				public final String userId;
				public final String userName;
				public final String userEmail;

				@JsonCreator
				public Actor(@JsonProperty("userId") String userId, @JsonProperty("userName") String userName,
						@JsonProperty("userEmail") String userEmail) {
					this.userId = userId;
					this.userName = userName;
					this.userEmail = userEmail;
				}
			}

			public static final class Participant {
				public final String userId;
				public final String userName;
				public final String userEmail;

				@JsonCreator
				public Participant(@JsonProperty("userId") String userId, @JsonProperty("userName") String userName,
						@JsonProperty("userEmail") String userEmail) {
					this.userId = userId;
					this.userName = userName;
					this.userEmail = userEmail;
				}
			}
		}
	}
}