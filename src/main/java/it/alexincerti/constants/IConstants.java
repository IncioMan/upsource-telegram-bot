package it.alexincerti.constants;

public interface IConstants {
	public static final String CONFIGURATION_PATH = "CONFIGURATION_FILE_PATH";
	public static final String CONFIGURATION_ENV = "file:${" + IConstants.CONFIGURATION_PATH + "}";
}
