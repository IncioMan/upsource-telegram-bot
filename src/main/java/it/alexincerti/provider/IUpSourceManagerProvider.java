package it.alexincerti.provider;

import it.alexincerti.manager.IUpSourceManager;

public interface IUpSourceManagerProvider {
	IUpSourceManager getManager(String dataType);
}
