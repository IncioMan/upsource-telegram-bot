package it.alexincerti.provider;

import org.springframework.stereotype.Component;

import it.alexincerti.model.UpSourceProperties;
import it.alexincerti.model.UpSourceUpdate;

@Component
public class UpSourceLinkCreator implements IUpSourceLinkCreator {

	private UpSourceProperties upSourceProperties;

	public UpSourceLinkCreator(UpSourceProperties upSourceProperties) {
		this.upSourceProperties = upSourceProperties;
	}

	@Override
	public String getLinkToComment(UpSourceUpdate update) {
		StringBuilder commentLink = new StringBuilder();

		commentLink.append(getLinkToReview(update));
		commentLink.append("?commentId=" + update.data.commentId);

		return commentLink.toString();
	}

	@Override
	public String getLinkToReview(UpSourceUpdate update) {
		StringBuilder reviewLink = new StringBuilder();

		reviewLink.append(getLinkToProject(update));
		reviewLink.append("review" + "/");
		reviewLink.append(update.data.base.reviewId);

		return reviewLink.toString();
	}

	@Override
	public String getLinkToProject(UpSourceUpdate update) {
		StringBuilder reviewLink = new StringBuilder();

		reviewLink.append(upSourceProperties.getInstallationUrl() + "/");
		reviewLink.append(update.projectId + "/");
		return reviewLink.toString();
	}

}
