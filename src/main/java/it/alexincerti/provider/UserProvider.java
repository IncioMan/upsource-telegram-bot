package it.alexincerti.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/*
 * Retrieve the user info from Property file
 */
@Component
public class UserProvider implements IUserProvider {

	@Autowired
	private Environment environment;

	public UserProvider() {
	}

	@Override
	public String getUserChatId(String username) {
		String chatId = null;

		try {
			chatId = environment.getProperty(username);
		} catch (Exception e) {
			System.err.println("UserProvider - Username provided invalid: " + username);
		}

		if (chatId == null) {
			System.err.println("UserProvider - ChatId not found for: " + username);
		}
		
		System.out.println("Chatid found for " + username + " : " + chatId);
		return chatId;
	}

}
