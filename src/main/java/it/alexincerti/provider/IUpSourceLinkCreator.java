package it.alexincerti.provider;

import it.alexincerti.model.UpSourceUpdate;

public interface IUpSourceLinkCreator {

	String getLinkToComment(UpSourceUpdate update);

	String getLinkToReview(UpSourceUpdate update);

	String getLinkToProject(UpSourceUpdate update);

}