package it.alexincerti.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.alexincerti.enums.DataType;
import it.alexincerti.manager.DiscussionFeedEventBeanUpSourceManager;
import it.alexincerti.manager.IUpSourceManager;
import it.alexincerti.manager.NewParticipantInReviewFeedEventBeanUpSourceManager;
import it.alexincerti.manager.ParticipantStateChangedFeedEventBeanUpSourceManager;
import it.alexincerti.manager.ReviewStateChangedFeedEventBeanUpSourceManager;

@Component
public class UpSourceManagerProvider implements IUpSourceManagerProvider {

	@Autowired
	private DiscussionFeedEventBeanUpSourceManager discussionFeedEventBeanUpSourceManager;

	@Autowired
	private NewParticipantInReviewFeedEventBeanUpSourceManager newParticipantInReviewFeedEventBeanUpSourceManager;

	@Autowired
	private ParticipantStateChangedFeedEventBeanUpSourceManager participantStateChangedFeedEventBeanUpSourceManager;

	@Autowired
	private ReviewStateChangedFeedEventBeanUpSourceManager reviewStateChangedFeedEventBeanUpSourceManager;

	@Override
	public IUpSourceManager getManager(String dataType) {
		switch (DataType.valueOf(dataType)) {
		case DiscussionFeedEventBean:
			return discussionFeedEventBeanUpSourceManager;
		case NewParticipantInReviewFeedEventBean:
			return newParticipantInReviewFeedEventBeanUpSourceManager;
		case ParticipantStateChangedFeedEventBean:
			return participantStateChangedFeedEventBeanUpSourceManager;
		case ReviewStateChangedFeedEventBean:
			return reviewStateChangedFeedEventBeanUpSourceManager;
		default:
			return null;
		}
	}
}
