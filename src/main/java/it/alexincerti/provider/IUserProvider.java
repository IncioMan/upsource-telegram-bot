package it.alexincerti.provider;

/*
 * Interface for providing info about users.
 * e.g the telegram chatId
 */
public interface IUserProvider {
	String PREFIX = "UPSOURCE_CHATID";

	public String getUserChatId(String username);
}
