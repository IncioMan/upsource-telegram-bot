package it.alexincerti.manager;

import org.springframework.beans.factory.annotation.Autowired;

import it.alexincerti.bot.AbstractBotController;
import it.alexincerti.provider.IUpSourceLinkCreator;
import it.alexincerti.provider.IUserProvider;

public abstract class AbstractUpSourceManager implements IUpSourceManager {
	
	protected AbstractBotController bot;
	
	@Autowired
	protected IUserProvider userProvider;

	@Autowired
	protected IUpSourceLinkCreator linkCreator;
	
	@Override
	public void setBot(AbstractBotController bot) {
		this.bot = bot;
	}
}
