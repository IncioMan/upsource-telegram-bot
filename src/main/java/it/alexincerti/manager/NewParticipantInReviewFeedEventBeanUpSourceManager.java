package it.alexincerti.manager;

import org.springframework.stereotype.Component;

import com.pengrad.telegrambot.model.request.ParseMode;

import it.alexincerti.enums.Role;
import it.alexincerti.model.UpSourceUpdate;
import it.alexincerti.model.UpSourceUpdate.Data.Base.UserId;
import it.alexincerti.provider.IUpSourceLinkCreator;
import it.alexincerti.utils.StyleText;

@Component
public class NewParticipantInReviewFeedEventBeanUpSourceManager extends AbstractUpSourceManager {

	private IUpSourceLinkCreator linkCreator;

	public NewParticipantInReviewFeedEventBeanUpSourceManager(IUpSourceLinkCreator linkCreator) {
		this.linkCreator = linkCreator;
	}

	@Override
	public void processUpdate(UpSourceUpdate update) {
		StringBuilder message = new StringBuilder();

		message.append("User "
				+ (update.data.base.actor.userName == null ? update.data.base.actor.userEmail
						: update.data.base.actor.userName)
				+ " has added "
				+ StyleText.bold((update.data.participant.userName == null ? update.data.participant.userEmail
						: update.data.participant.userName))
				+ " on the review  " + update.data.base.reviewId + " of " + StyleText.bold(update.projectId)
				+ " with the role: " + StyleText.bold(Role.getNameByValue(update.data.role + "").name()));

		message.append("\n\n" + linkCreator.getLinkToReview(update));

		for (UserId u : update.data.base.userIds) {
			if (u.userName == null) {
				continue;
			}

			// Not sending the update message to the user who performed the
			// action
			if (u.userName.equals(update.data.base.actor.userName)) {
				continue;
			}
			String userChatId = userProvider.getUserChatId(u.userName);

			if (userChatId == null) {
				continue;
			}
			bot.sendMessage(userChatId, message.toString(), ParseMode.HTML);
		}
	}
}
