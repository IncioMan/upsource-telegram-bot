package it.alexincerti.manager;

import org.springframework.stereotype.Component;

import com.pengrad.telegrambot.model.request.ParseMode;

import it.alexincerti.bot.AbstractBotController;
import it.alexincerti.model.UpSourceUpdate;
import it.alexincerti.model.UpSourceUpdate.Data.Base.UserId;
import it.alexincerti.utils.StyleText;

@Component
public class DiscussionFeedEventBeanUpSourceManager extends AbstractUpSourceManager implements IUpSourceManager {

	private AbstractBotController bot;

	@Override
	public void processUpdate(UpSourceUpdate update) {
		StringBuilder message = new StringBuilder();

		message.append("\nThe user " + StyleText.bold((update.data.base.actor.userName == null
				? update.data.base.actor.userEmail : update.data.base.actor.userName)));
		message.append(" has commented");
		
		if (update.data.base.reviewId == null) {
			message.append(" a review");
		} else {
			message.append(" the review " + StyleText.bold(update.data.base.reviewId));
			
		}
		message.append(" of the project: " + StyleText.bold(update.projectId));
		// NotificationReason notificationName =
		// NotificationReason.getNameByValue(update.data.notificationReason +
		// "");
		// if (!notificationName.equals(NotificationReason.Unknown)) {
		// message.append("\nNotification reason: " + notificationName);
		// }

		if(update.data.commentId != null){
		message.append("\nComment: " + StyleText.bold(StyleText.styleTag(update.data.commentText)));
		}
		
		if (update.data.commentId != null || update.data.base.reviewId == null) {
			message.append("\n\nGo to the review with the following link:\n\n<a href=\""
					+ linkCreator.getLinkToProject(update) + "\">" + linkCreator.getLinkToProject(update) + "</a>");
		} else {
			// Link to comment
			message.append("\n\nGo to the comment with the following link:\n\n<a href=\""
					+ linkCreator.getLinkToComment(update) + "\">" + linkCreator.getLinkToComment(update) + "</a>");
		}

		for (UserId u : update.data.base.userIds) {
			if (u.userName == null) {
				continue;
			}
			if (u.userName.equals(update.data.base.actor.userName)) {
				continue;
			}
			String userChatId = userProvider.getUserChatId(u.userName);

			if (userChatId == null) {
				continue;
			}
			bot.sendMessage(userChatId, message.toString(), ParseMode.HTML);
		}
	}

	@Override
	public void setBot(AbstractBotController bot) {
		this.bot = bot;
	}
}
