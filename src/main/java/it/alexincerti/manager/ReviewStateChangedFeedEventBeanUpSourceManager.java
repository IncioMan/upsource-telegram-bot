package it.alexincerti.manager;

import org.springframework.stereotype.Component;

import com.pengrad.telegrambot.model.request.ParseMode;

import it.alexincerti.enums.ReviewState;
import it.alexincerti.model.UpSourceUpdate;
import it.alexincerti.model.UpSourceUpdate.Data.Base.UserId;
import it.alexincerti.utils.StyleText;

@Component
public class ReviewStateChangedFeedEventBeanUpSourceManager extends AbstractUpSourceManager
		implements IUpSourceManager {

	@Override
	public void processUpdate(UpSourceUpdate update) {
		StringBuilder message = new StringBuilder();

		message.append("User "
				+ StyleText.bold((update.data.base.actor.userName == null ? update.data.base.actor.userEmail
						: update.data.base.actor.userName))
				+ " has changed the state of");
		if (update.data.base.reviewId == null) {
			message.append(" a review");
		} else {
			message.append(" the review " + StyleText.bold(update.data.base.reviewId));
		}
		message.append(StyleText.bold(update.projectId) + " from "
				+ StyleText.bold(ReviewState.getNameByValue(update.data.oldState + "").name()) + " to "
				+ StyleText.bold(ReviewState.getNameByValue(update.data.newState + "").name()));
		
		if (update.data.base.reviewId == null) {
			message.append("\n\nGo to the review with the following link:\n\n<a href=\""
					+ linkCreator.getLinkToProject(update) + "\">" + linkCreator.getLinkToProject(update) + "</a>");
		} else {
			// Link to comment
			message.append("\n\nGo to the review with the following link:\n\n<a href=\""
					+ linkCreator.getLinkToReview(update) + "\">" + linkCreator.getLinkToReview(update) + "</a>");
		}
		
		for (UserId u : update.data.base.userIds) {
			if (u.userName == null) {
				continue;
			}

			// Not sending the update message to the user who performed the
			// action
			if (u.userName.equals(update.data.base.actor.userName)) {
				continue;
			}
			String userChatId = userProvider.getUserChatId(u.userName);

			if (userChatId == null) {
				continue;
			}
			bot.sendMessage(userChatId, message.toString(), ParseMode.HTML);
		}
	}
}
