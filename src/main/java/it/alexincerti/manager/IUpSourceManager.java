package it.alexincerti.manager;

import it.alexincerti.bot.AbstractBotController;
import it.alexincerti.model.UpSourceUpdate;

public interface IUpSourceManager {
	void processUpdate(UpSourceUpdate update);

	void setBot(AbstractBotController bot);
}
