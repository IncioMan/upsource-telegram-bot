package it.alexincerti.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StyleText {

	public static String bold(String text) {
		return "<b>" + text + "</b>";
	}

	public static String styleTag(String text) {
		if (!text.contains("@")) {
			return text;
		}

		Pattern pattern = Pattern.compile("@[^,;]+,");
		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {
			text = text.replace(matcher.group(), "");
		}

		text = text.replace("\\{", "");
		text = text.replace("}", "");

		return text;
	}

}
