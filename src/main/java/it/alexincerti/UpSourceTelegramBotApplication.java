package it.alexincerti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import it.alexincerti.constants.IConstants;

@SpringBootApplication
@EnableConfigurationProperties
@PropertySource(value = { IConstants.CONFIGURATION_ENV + "/users.properties" }, ignoreResourceNotFound = false)
public class UpSourceTelegramBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(UpSourceTelegramBotApplication.class, args);
	}
}
