package it.alexincerti.bot;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.model.request.ReplyKeyboardHide;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.AnswerInlineQuery;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SendPhoto;

import it.alexincerti.scheduler.SchedulerManager;

public abstract class AbstractBotController {
	protected TelegramBot bot;

	protected TelegramBot debugBot;

	@Autowired
	protected SchedulerManager schedulerManager;

	protected void initializeBot(String botToken) {
		bot = TelegramBotAdapter.build(botToken);
	}

	protected void initializeDebugBot(String botToken) {
		debugBot = TelegramBotAdapter.build(botToken);
	}

	protected String getBody(BufferedReader reader) throws IOException {
		String line = null;
		StringBuilder builder = new StringBuilder();

		while ((line = reader.readLine()) != null)
			builder.append(line);

		return builder.toString();
	}

	public void sendMessage(String chatId, String message) {
		bot.execute(new SendMessage(chatId, message).replyMarkup(new ReplyKeyboardHide()));
	}

	public void sendMessage(String chatId, String message, ParseMode parseMode) {
		bot.execute(new SendMessage(chatId, message).replyMarkup(new ReplyKeyboardHide()).parseMode(parseMode));
	}

	public void sendMessage(String chatId, String message, ReplyKeyboardMarkup replyKeyboardMarkup) {
		bot.execute(new SendMessage(chatId.toString(), message).replyMarkup(replyKeyboardMarkup));
	}

	public void sendAnswerInlineQuery(AnswerInlineQuery answerInlineQuery) {
		bot.execute(answerInlineQuery);
	}

	public void sendPhoto(String chatId, File file) {
		bot.execute(new SendPhoto(chatId, file));
	}

	public void debugMessage(String chatId, String text) {
		debugBot.execute(new SendMessage(chatId, text));
	}
}
