package it.alexincerti.bot;

import javax.annotation.PostConstruct;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import it.alexincerti.manager.IUpSourceManager;
import it.alexincerti.model.UpSourceUpdate;
import it.alexincerti.provider.IUpSourceManagerProvider;

@Controller
@RequestMapping(path = "/")
@ConfigurationProperties(prefix = "bot")
public class UpSourceBot extends AbstractBotController {

	/**
	 * The Bot's token, provided by Botfather Needed to send message and all the
	 * related actions with the Telegram Bot
	 */
	private String tokenId;

	private IUpSourceManagerProvider provider;

	public UpSourceBot(IUpSourceManagerProvider provider) {
		this.provider = provider;
	}

	@PostConstruct
	private void initializeBot() {
		super.initializeBot(tokenId);
	}

	@PostMapping
	public String upSourceUpdate(@RequestBody UpSourceUpdate update) {
		if (update == null) {
			return "home";
		}

		System.out.println("Updated received: " + update.toString());

		IUpSourceManager manager = provider.getManager(update.dataType);
		manager.setBot(this);
		manager.processUpdate(update);
		
		return "home";
	}

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

}
