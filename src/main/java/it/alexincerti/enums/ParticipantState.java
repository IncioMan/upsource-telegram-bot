package it.alexincerti.enums;

public enum ParticipantState {
	Unread("0"), Read("1"), Accepted("2"), Rejected("3");

	private String value;

	private ParticipantState(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	private void setValue(String value) {
		this.value = value;
	}

	public static ParticipantState getNameByValue(String code) {
		for (ParticipantState e : ParticipantState.values()) {
			if (code.equals(e.getValue()))
				return e;
		}
		return null;
	}
}
