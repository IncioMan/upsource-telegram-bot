package it.alexincerti.enums;

public enum Role {
	Author("1"), Reviewer("2"), Watcher("3");

	private String value;

	private Role(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	private void setValue(String value) {
		this.value = value;
	}

	public static Role getNameByValue(String code) {
		for (Role e : Role.values()) {
			if (code.equals(e.getValue()))
				return e;
		}
		return null;
	}
}
