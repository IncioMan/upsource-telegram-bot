package it.alexincerti.enums;

public enum NotificationReason {
	Unknown("0"), CommentInAuthorFeed("1"), NotifyCommitAuthor("2"), Mention("3"), HashTagSubscription(
			"4"), DiscussionIsStarred("5"), ParticipatedInDiscussion("6"), ParticipatedInReview("7"), Reply("8");

	private String value;

	private NotificationReason(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	private void setValue(String value) {
		this.value = value;
	}

	public static NotificationReason getNameByValue(String code) {
		for (NotificationReason e : NotificationReason.values()) {
			if (code.equals(e.getValue()))
				return e;
		}
		return null;
	}
}
