package it.alexincerti.enums;

public enum ReviewState {
	Open("0"), 	
	Closed("1");
	
	private String value;

	private ReviewState(String value) {
		this.setValue(value);
	}

	public String getValue() {
		return value;
	}

	private void setValue(String value) {
		this.value = value;
	}

	public static ReviewState getNameByValue(String code) {
		for (ReviewState e : ReviewState.values()) {
			if (code.equals(e.getValue()))
				return e;
		}
		return null;
	}
}
