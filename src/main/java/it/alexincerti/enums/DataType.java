package it.alexincerti.enums;

/*
 * Enum for the event type provided by UpSource
 */
public enum DataType {
	DiscussionFeedEventBean, NewParticipantInReviewFeedEventBean, ParticipantStateChangedFeedEventBean, ReviewStateChangedFeedEventBean;
}
